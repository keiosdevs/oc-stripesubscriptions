<?php
Route::group(
    ['prefix' => 'api/v1', 'middleware' => ['web']],
    function () {
        Route::any(
            '/subscriptions',
            function () {
                $data = Input::all();
                \Log::info('WEBHOOK: '. print_r($data, true));
                $controller = new \Viamage\StripeSubscriptions\Classes\ApiController();
                try {
                    $controller->processWebhookRequest($data);

                    return \Response::json(['message' => 'OK', 200]);
                } catch (\Exception $e) {
                    \Log::error($e->getMessage() . ' '. $e->getTraceAsString());
                    return \Response::json(['message' => $e->getMessage()], 500);
                }
            }
        );
        Route::any(
            '/sth',
        function(){
            return \Response::json(['message' => 'OK', 200]);
        });
        Route::post(
            '/subscribe',
            function () {
                $data = Input::all();
                \Log::info('SETUP: '. print_r($data, true));
                $controller = new \Viamage\StripeSubscriptions\Classes\ApiController();

                return $controller->subscribe($data);
            }
        );
        Route::get(
            '/subscribe',
            function () {
                $data = Input::all();
                \Log::info('SETUP: '. print_r($data, true));
                $controller = new \Viamage\StripeSubscriptions\Classes\ApiController();

                return $controller->subscribe($data, true);
            }
        );
    }
);