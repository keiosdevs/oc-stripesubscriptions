<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 5/1/18
 * Time: 9:29 PM
 */

namespace Viamage\StripeSubscriptions\Jobs;

use Keios\Apparatus\Classes\JobManager;
use Keios\Apparatus\Contracts\ApparatusQueueJob;
use Keios\PaymentGateway\Models\Settings;
use Keios\PaymentGateway\Models\Subscription;
use Viamage\RealTime\Classes\Pusher;

/**
 * Class CancelJob
 * @package Viamage\StripeSubscriptions\Jobs
 */
class CancelJob implements ApparatusQueueJob
{
    /**
     * @var int
     */
    public $jobId;

    /**
     * @var JobManager
     */
    public $jobManager;

    /**
     * @var \Viamage\StripeSubscriptions\Models\Subscription
     */
    private $subscriptionModel;

    /**
     * @var bool
     */
    private $testMode;
    /**
     * @var string
     */
    private $sapiKey;
    /**
     * @var string
     */
    private $stestApiKey;

    /**
     * @param int $id
     */
    public function assignJobId(int $id)
    {
        $this->jobId = $id;
    }

    /**
     * Cancel job constructor constructor.
     *
     * Cancels subscription in background
     *
     * @param $subscriptionId
     * @param $subscriptionModel
     * @param $testMode
     * @internal param array $rates
     * @internal param bool $updateExisting
     * @internal param int $chunk
     */
    public function __construct($subscriptionId, $subscriptionModel, $testMode)
    {
        $stripeSettings = Settings::instance();
        $this->testMode = (bool)$stripeSettings->get('stripe.testMode');
        $this->sapiKey = $stripeSettings->get('stripe.sapiKey');
        $this->stestApiKey = $stripeSettings->get('stripe.stestApiKey');
        $this->subscriptionModel = $subscriptionModel;
        $this->testMode = $testMode;
    }

    /**
     * Job handler. This will be done in background.
     *
     * @param JobManager $jobManager
     */
    public function handle(JobManager $jobManager): void
    {
        $jobManager->startJob($this->jobId, 1);
        try {
            $subscriptionKey = $this->subscriptionModel->subscription_id;
            $this->setStripeKey();
            $subscription = \Stripe\Subscription::retrieve($subscriptionKey);
            $subscription->cancel(['at_period_end' => true]);
        } catch (\Exception $e) {
            $jobManager->failJob($this->jobId, ['error' => $e->getMessage()]);
            throw $e;
        }
        $jobManager->completeJob($this->jobId,[]);
    }

    /**
     *
     */
    private function setStripeKey()
    {
        if ($this->testMode) {
            \Stripe\Stripe::setApiKey($this->stestApiKey);
        } else {
            \Stripe\Stripe::setApiKey($this->sapiKey);
        }

    }
}