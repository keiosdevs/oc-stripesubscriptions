<?php namespace Viamage\StripeSubscriptions\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Viamage\StripeSubscriptions\Models\Subscription;

/**
 * Subscriptions Back-end Controller
 */
class Subscriptions extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Viamage.StripeSubscriptions', 'stripesubscriptions', 'subscriptions');
    }

    /**
     * Deleted checked subscriptions.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $subscriptionId) {
                if (!$subscription = Subscription::find($subscriptionId)) {
                    continue;
                }
                $subscription->delete();
            }

            Flash::success(Lang::get('viamage.stripesubscriptions::lang.subscriptions.delete_selected_success'));
        } else {
            Flash::error(Lang::get('viamage.stripesubscriptions::lang.subscriptions.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
