<?php namespace Viamage\StripeSubscriptions;

use Backend;
use System\Classes\PluginBase;
use Viamage\StripeSubscriptions\Classes\StripeSubscriptionOrderEventSubscriber;
use Viamage\StripeSubscriptions\Classes\StripeSubscriptionServiceProvider;
use Viamage\StripeSubscriptions\Classes\SubscriptionItem;
use Viamage\StripeSubscriptions\Classes\SubscriptionItemFormatter;
use Viamage\StripeSubscriptions\Components\Subscribe;
use Keios\PaymentGateway\Core\SerializationService;
use Keios\PaymentGateway\Facades\ItemFormatter;

/**
 * StripeSubscriptions Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = [
        'Keios.PaymentGateway',
        'Keios.PgStripe'
    ];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'StripeSubscriptions',
            'description' => 'No description provided yet...',
            'author'      => 'Viamage',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        /**
         * @var Dispatcher $dispatcher
         */
        \Event::listen(
            'paymentgateway.booted',
            function () {
                ItemFormatter::add(SubscriptionItem::TYPE, SubscriptionItemFormatter::class);
                //ItemFormatter::useForNewQuoteItems(RechargeShopItem::class);
                $cfg = $this->app['config']->get('keios.paymentgateway.items.types');
                $newCfg = [];
                foreach ($cfg as $type => $className) {
                    if ($type !== 'ADJUSTABLE') {
                        $newCfg[$type] = $className;
                    } else {
                        $newCfg[$type] = SubscriptionItemFormatter::class;
                    }
                }
                $this->app['config']->set('keios.paymentgateway.items.types', $newCfg);
            }
        );
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        $this->app->register(StripeSubscriptionServiceProvider::class);

        $this->app['events']->listen(
            SerializationService::class,
            function (SerializationService $service) {
                $service->registerMetadataDirectory(
                    plugins_path('viamage/stripesubscriptions/metadata'),
                    'Viamage.StripeSubscriptions'
                );
            }
        );
    }

    /**
     * @param boolean $loadedYamlConfigration
     */
    public function setLoadedYamlConfigration($loadedYamlConfigration)
    {
        $this->loadedYamlConfigration = $loadedYamlConfigration;
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            Subscribe::class => 'vm_subscribe',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'viamage.stripesubscriptions.some_permission' => [
                'tab' => 'StripeSubscriptions',
                'label' => 'Some permission'
            ],
        ];
    }


    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'stripesubscriptions' => [
                'label'       => 'Subscriptions',
                'url'         => Backend::url('viamage/stripesubscriptions/subscriptions'),
                'icon'        => 'icon-leaf',
                'permissions' => ['viamage.stripesubscriptions.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'subscriptions' => [
                        'label'       => 'Subscriptions',
                        'icon'        => 'icon-money',
                        'url'         => Backend::url('viamage/stripesubscriptions/subscriptions'),
                        'permissions' => ['viamage.stripesubscriptions.subscriptions'],
                    ],
                    'products' => [
                        'label'       => 'Products',
                        'icon'        => 'icon-cube',
                        'url'         => Backend::url('viamage/stripesubscriptions/products'),
                        'permissions' => ['viamage.stripesubscriptions.products'],
                    ],

                ],
            ],
        ];
    }
}
