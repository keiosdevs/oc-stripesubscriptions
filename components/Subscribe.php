<?php namespace Viamage\StripeSubscriptions\Components;

use Cms\Classes\CodeBase;
use Cms\Classes\ComponentBase;
use Keios\PaymentGateway\Models\Settings;
use Stripe\Subscription as StripeSubscription;
use Viamage\StripeSubscriptions\Jobs\CancelJob;
use Viamage\StripeSubscriptions\Models\Product;
use Viamage\StripeSubscriptions\Models\Subscription;
use Keios\Apparatus\Classes\JobManager;

/**
 * Class Subscribe
 * @package Viamage\StripeSubscriptions\Components
 */
class Subscribe extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name'        => 'Subscribe Component',
            'description' => 'No description provided yet...',
        ];
    }

    /**
     * @var
     */
    private $apiKey;
    private $sapiKey;
    private $testApiKey;
    private $stestApiKey;
    private $testMode;

    /**
     * Subscribe constructor.
     * @param null  $cmsObject
     * @param array $properties
     */
    public function __construct($cmsObject = null, array $properties = [])
    {
        parent::__construct($cmsObject, $properties);
        $stripeSettings = Settings::instance();
        $this->apiKey = $stripeSettings->get('stripe.apiKey');
        $this->testApiKey = $stripeSettings->get('stripe.testApiKey');
        $this->testMode = $stripeSettings->get('stripe.testMode');
    }

    /**
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     *
     */
    public function onRun()
    {
        $this->page['products'] = Product::where('is_active', true)->orderBy('value', 'asc')->where('is_active', true)->rememberForever('stripe_sub_products')->get();
        if ($this->testMode) {
            $this->page['stripe_key'] = $this->testApiKey;
        } else {
            $this->page['stripe_key'] = $this->apiKey;
        }
     }

    /**
     *
     */
    public function onLoadForm()
    {
        $user = \Auth::getUser();
        $this->page['email'] = $user->email;
        if ($this->testMode) {
            $this->page['stripe_key'] = $this->testApiKey;
        } else {
            $this->page['stripe_key'] = $this->apiKey;
        }
        $productId = post('product');
        if($productId) {
            $this->page['product'] = $product = Product::where('id', $productId)->first();
            \Session::put('plan_key', $product->stripe_plan_key);
        }
    }

    public function onCancelSubscription()
    {
        $data = post();
        $subscriptionId = $data['subscription_id'];
        $subscriptionModel = Subscription::where('id', $subscriptionId)->first();
        $job = new CancelJob($subscriptionId, $subscriptionModel, $this->testMode);
        /** @var JobManager $jobManager */
        $jobManager = \App::make(JobManager::class);
        $jobManager->setSimpleJob(true);
        $jobManager->dispatch($job, 'Canceling subscription ' . $subscriptionId);
    }


}
