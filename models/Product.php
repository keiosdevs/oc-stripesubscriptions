<?php namespace Viamage\StripeSubscriptions\Models;

use Keios\MoneyRight\Money;
use Keios\PaymentGateway\Models\Settings;
use Model;
use System\Models\File;

/**
 * Product Model
 */
class Product extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_stripesubscriptions_products';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'cover' => File::class,
    ];
    public $attachMany = [];

    public function afterSave(){
        \Cache::forget('stripe_sub_products');
    }

    public function afterCreate(){
        \Cache::forget('stripe_sub_products');
    }

    public function getValue(){
        $settings = Settings::instance();
        $currency = $settings->get('defaultCurrency');
        return Money::$currency($this->value / 100);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getValueMultiplied()
    {
        return (int)$this->value;
    }

    public function getButtonLabel()
    {
        return $this->button_label;
    }

    public function getImagePath()
    {
        if ($this->cover) {
            return $this->cover->path;
        }

        return null;
    }
}
