<?php namespace Viamage\StripeSubscriptions\Models;

use Keios\PaymentGateway\Models\Payment;
use Keios\ProUser\Models\User;
use Model;

/**
 * Subscription Model
 */
class Subscription extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_stripesubscriptions_subscriptions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'user' => User::class,
    ];
    public $belongsToMany = [
        'payments' => [
            Payment::class,
            'table'    => 'viamage_stripesubscriptions_pay_pivot',
            'key'      => 'subscription_id',
            'otherKey' => 'payment_id',
        ],
    ];
}
