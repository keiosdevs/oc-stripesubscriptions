<?php namespace Viamage\StripeSubscriptions\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_stripesubscriptions_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('button_label')->default('Sign me up!');
            $table->string('description');
            $table->double('value')->default(0.00);
            $table->string('stripe_plan_key')->index();
            $table->boolean('is_active')->default(false);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_stripesubscriptions_products');
    }
}
