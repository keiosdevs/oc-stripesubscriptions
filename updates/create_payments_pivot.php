<?php namespace Viamage\StripeSubscriptions\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePaymentsPivot extends Migration
{
    public function up()
    {
        Schema::create('viamage_stripesubscriptions_pay_pivot', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('payment_id')->index();
            $table->integer('subscription_id')->index();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_stripesubscriptions_pay_pivot');
    }
}
