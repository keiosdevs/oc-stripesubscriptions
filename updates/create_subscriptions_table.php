<?php namespace Viamage\StripeSubscriptions\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSubscriptionsTable extends Migration
{
    public function up()
    {
        Schema::create(
            'viamage_stripesubscriptions_subscriptions',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('subscription_id')->index();
                $table->integer('user_id')->index();
                $table->boolean('is_active')->default(false);
                $table->dateTime('current_period_start')->nullable();
                $table->dateTime('current_period_end')->nullable();
                $table->string('billing')->nullable();
                $table->text('metadata')->nullable();
                $table->boolean('has_trial')->default(false);
                $table->dateTime('trial_start')->nullable();
                $table->dateTime('trial_end')->nullable();
                $table->dateTime('cancel_at')->nullable();
                $table->dateTime('canceled_at')->nullable();
                $table->integer('payment_id')->nullable()->index();
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('viamage_stripesubscriptions_subscriptions');
    }
}
