<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/29/18
 * Time: 6:21 PM
 */

namespace Viamage\StripeSubscriptions\Classes;

use Keios\MoneyRight\Money;
use Keios\PaymentGateway\Core\Operator;
use Keios\PaymentGateway\ValueObjects\Cart;
use Keios\PaymentGateway\ValueObjects\Item;
use Viamage\StripeSubscriptions\Contracts\CartStoreInterface;
use Keios\PaymentGateway\ValueObjects\Details;
use October\Rain\Auth\Models\User;

/**
 * Class CartManager
 * @package Viamage\StripeSubscriptions\Classes
 */
class CartManager
{

    /**
     * @var \Viamage\StripeSubscriptions\Contracts\CartStoreInterface
     */
    protected $store;

    /**
     * @var \Keios\PaymentGateway\ValueObjects\Cart
     */
    protected $cart;

    /**
     * @var \Viamage\StripeSubscriptions\Classes\CartManager
     */
    protected $manager;

    /**
     * CartManager constructor.
     *
     * @param CartStoreInterface $store
     */
    public function __construct(CartStoreInterface $store)
    {
        $this->store = $store;
        $this->cart = $this->store->load();
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->cart->getItemCount();
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param array $ids
     */
    public function removeProducts(array $ids)
    {
        foreach ($ids as $id) {
            $this->cart->remove($id);
        }

        $this->store->save($this->cart);
    }

    /**
     * @param array   $product
     * @param integer $amount
     * @param array   $discount
     *
     * @return bool
     */
    public function addProduct($subscription)
    {
        $generatedItem = $this->prepareItem($subscription);
        $this->cart->add($generatedItem);
        $this->store->save($this->cart);

        return true;
    }

    /**
     * @param User $user
     *
     * @return Operator
     * @throws \ApplicationException
     */
    public function makePayment(User $user, $description, $chargeId)
    {
        if ($this->cart->isEmpty()) {
            throw new \ApplicationException('Cart is empty');
        }

        $details = new Details(['chargeId' => $chargeId], 'keios.pgstripe::lang.operators.stripe', $description, null, $user->email);

        $operator = \PaymentManager::create($this->cart, $details, $user);

        $this->store->delete();

        return $operator;
    }

    /**
     * @param $subscription
     * @return \Keios\PaymentGateway\Contracts\Purchasable
     */
    private function prepareItem($subscription)
    {
        $worth = $this->getMoneyWorth($subscription);

        return new Item($subscription->tier, 1, $worth, 0, []); // todo vat
    }

    /**
     * @param $subscription
     * @return Money
     */
    private function getMoneyWorth($subscription)
    {

    }
}