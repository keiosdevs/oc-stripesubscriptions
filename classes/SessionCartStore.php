<?php namespace Viamage\StripeSubscriptions\Classes;

use Illuminate\Session\SessionManager;
use Keios\PaymentGateway\ValueObjects\Cart;
use Viamage\StripeSubscriptions\Contracts\CartStoreInterface;

/**
 * Class SessionCartStore
 *
 * @package Keios\RechargeShop
 */
class SessionCartStore implements CartStoreInterface
{

    /**
     * @constant
     */
    const SESSION_KEY = 'RechargeShop.Cart';

    /**
     * @var \Illuminate\Session\SessionManager
     */
    protected $sessionStore;

    /**
     * @param \Illuminate\Session\SessionManager $sessionStore
     */
    public function __construct(SessionManager $sessionStore)
    {
        $this->sessionStore = $sessionStore;
    }


    /**
     * @param Cart $cart
     *
     * @return bool
     */
    public function save(Cart $cart)
    {
        $this->sessionStore->put(self::SESSION_KEY, $cart);
        return true;
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\Cart
     */
    public function load()
    {
        $cart = $this->sessionStore->get(self::SESSION_KEY);

        if (!$cart) {
            $cart = new Cart();
        }

        return $cart;
    }

    /**
     * @return null
     */
    public function delete()
    {
        $this->sessionStore->forget(self::SESSION_KEY);
    }
}