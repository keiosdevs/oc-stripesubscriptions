<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/29/18
 * Time: 1:33 PM
 */

namespace Viamage\StripeSubscriptions\Classes;

use Carbon\Carbon;
use Event;
use Illuminate\Http\RedirectResponse;
use Keios\MoneyRight\Money;
use Keios\PaymentGateway\Models\Settings;
use Keios\ProUser\Models\User;
use Stripe\Card;
use Stripe\Invoice;
use Viamage\StripeSubscriptions\Models\Product;
use Viamage\StripeSubscriptions\Models\Subscription;

/**
 * Class ApiController
 * @package Viamage\StripeSubscriptions\Classes
 */
class ApiController
{

    /**
     * @var
     */
    private $apiKey;
    private $sapiKey;
    private $testApiKey;
    private $stestApiKey;
    private $testMode;
    private $settings;
    private $currency;
    private $webhookSecret;
    private $errorRedirect;
    private $successRedirect;

    /**
     * Subscribe constructor.
     */
    public function __construct()
    {
        $stripeSettings = $this->settings = Settings::instance();
        $this->sapiKey = $stripeSettings->get('stripe.sapiKey');
        $this->stestApiKey = $stripeSettings->get('stripe.stestApiKey');
        $this->testMode = $stripeSettings->get('stripe.testMode');
        $this->currency = $this->settings->get('defaultCurrency');
        $this->webhookSecret = $this->settings->get('stripe.webhookSecret');
        $this->successRedirect = $this->settings->get('stripe.successRedirect', '/');
        $this->errorRedirect = $this->settings->get('stripe.errorRedirect', '/');
    }

    public function subscribe(array $data, bool $secureReturn = false): RedirectResponse
    {

        $user = \Auth::getUser();
        $this->setStripeKey();
        $planKey = \Session::get('plan_key');
        if ($secureReturn) {
            return $this->finishSecureSubscription($data, $user, $planKey);
        }
        if (array_key_exists('stripeToken', $data)) {
            return $this->processTokenSubscription($data, $user, $planKey);
        }

        if (array_key_exists('stripeSource', $data)) {
            return $this->processSourceSubscription($data, $user, $planKey);
        }

    }

    private function processSourceSubscription(array $data, User $user, string $planKey): RedirectResponse
    {
        /** @var Product $product */
        $product = Product::where('stripe_plan_key', $planKey)->first();
        $currencyIso = $this->settings->get('defaultCurrency');
        \Session::put('stripe_source', $data['stripeSource']);
        if ($data['stripeSecure'] === 'required' || $data['stripeSecure'] === 'optional') {
            $source = \Stripe\Source::create(
                [
                    'amount'         => $product->getValueMultiplied(),
                    'currency'       => strtolower($currencyIso),
                    'type'           => 'three_d_secure',
                    'three_d_secure' => [
                        'card' => $data['stripeSource'],
                    ],
                    'redirect'       => [
                        'return_url' => \Config::get('app.url').'/api/v1/subscribe',
                    ],
                ]
            );
            if ($source->redirect) {
                return \Redirect::to($source->redirect->url);
            }
        } else {
            return $this->finishSecureSubscription(['source' => $data['stripeSource']], $user, $planKey);
        }
    }

    private function finishSecureSubscription(array $data, User $user, string $planKey): RedirectResponse
    {
        $subscriptionModel = null;
        /** @var Product $product */
        $product = Product::where('stripe_plan_key', $planKey)->first();
        try {
            try {
                $customer = \Stripe\Customer::create(
                    [
                        'email'  => $user->getEmail(),
                        'source' => $data['source'],
                    ]
                );

                $charge = \Stripe\Charge::create(
                    [
                        'amount'   => $product->getValueMultiplied(),
                        'currency' => $this->currency,
                        'customer' => $customer->id,
                        'source'   => $data['source'],
                        'metadata' => ['email' => $user->getEmail()],
                    ]
                );

                $subscription = \Stripe\Subscription::create(
                    [
                        'source'    => \Session::get('stripe_source'),
                        'customer'  => $customer->id,
                        'items'     => [['plan' => $planKey]],
                        'trial_end' => Carbon::now()->addMonth()->timestamp,
                        'metadata' => ['email' => $user->getEmail()],
                    ]
                );
            } catch (\Exception $e) {
                \Log::error($e->getMessage().' '.$e->getTraceAsString());
                \Flash::error($e->getMessage());

                return \Redirect::to($this->errorRedirect);
            }
            $subscriptionModel = $this->createSubscriptionModel($subscription, $user);
            \Event::fire('viamage.stripe.new_subscription', [$user, $planKey, $subscriptionModel]);

            \Flash::success(trans('viamage.stripesubscriptions::lang.messages.successfully_subscribed'));

            return \Redirect::to($this->successRedirect);

        } catch (\Exception $e) {
            \Log::error(
                'unable to sign up customer:'.$_POST['stripeEmail'].
                ', error:'.$e->getMessage().' '.$e->getTraceAsString()
            );
            if ($subscriptionModel) {
                $subscription = \Stripe\Subscription::retrieve($subscriptionModel->subscription_id);
                $subscription->cancel(['at_period_end' => false]);
                \Log::info('Failed subscription canceled properly');
            }

            // TODO INFORM ! this is serious and subscription may be still active

            return \Redirect::to($this->errorRedirect);
        }
    }

    private function processTokenSubscription(array $data, User $user, string $planKey): RedirectResponse
    {
        $subscriptionModel = null;
        try {
            $customer = \Stripe\Customer::create(
                [
                    'email'  => $data['stripeEmail'],
                    'source' => $data['stripeToken'],
                ]
            );

            try {

                $subscription = \Stripe\Subscription::create(
                    [
                        'customer' => $customer->id,
                        'items'    => [['plan' => $planKey]],
                        'metadata' => ['email' => $user->getEmail()],
                    ]
                );
            } catch (\Exception $e) {
                \Log::error($e->getMessage().' '.$e->getTraceAsString());
                \Flash::error($e->getMessage());

                return \Redirect::to($this->errorRedirect);
            }
            $subscriptionModel = $this->createSubscriptionModel($subscription, $user);
            \Event::fire('viamage.stripe.new_subscription', [$user, $planKey, $subscriptionModel]);

            \Flash::success(trans('viamage.stripesubscriptions::lang.messages.successfully_subscribed'));

            return \Redirect::to($this->successRedirect);

        } catch (\Exception $e) {
            \Log::error(
                'unable to sign up customer:'.$_POST['stripeEmail'].
                ', error:'.$e->getMessage().' '.$e->getTraceAsString()
            );
            if ($subscriptionModel) {
                $subscription = \Stripe\Subscription::retrieve($subscriptionModel->subscription_id);
                $subscription->cancel(['at_period_end' => false]);
                \Log::info('Failed subscription canceled properly');
            }

            // TODO INFORM ! this is serious and subscription may be still active

            return \Redirect::to($this->errorRedirect);
        }
    }

    public function processWebhookRequest(array $postData): void
    {
        if (!array_key_exists('data', $postData)) {
            throw new \RuntimeException('Invalid hook payload');
        }
        $data = $postData['data'];
        if (array_key_exists('object', $data)) {
            $object = $data['object'];
            if ($object['object'] === 'subscription') {
                $this->processSubscriptionHook($object);
            }
            if ($object['object'] === 'charge') {
                $this->processChargeHook($object, $postData['type']);
            }
        }
    }

    private function processChargeHook($object, $type): void
    {
        /** @var CartManager $cartManager */
        $cartManager = \App::make('stripesubscription.cartmanager');
        $cart = $cartManager->getCart();

        $currency = $object['currency'];
        $amount = $object['amount'];
        $netPrice = Money::$currency($amount);
        $description = $this->generateChargeDescription($object);
        $item = new SubscriptionItem(
            $description,
            1,
            $netPrice,
            0,
            ['remote_id' => $object['id'], 'stripe_invoice' => $object['invoice']],
            []
        );
        $cart->add($item);
        $mail = null;
        if (array_key_exists('name', $object['source'])) {
            $mail = $object['source']['name'];
        }
        if (array_key_exists('email', $object['metadata'])) {
            $mail = $object['metadata']['email'];
        }
        if ($mail) {
            $user = User::where('email', $mail)->first();
        } else {
            $user = User::where('id', '>', 0)->first(); // TODO
        }
        $operator = $cartManager->makePayment($user, $description, $object['id']);
        $success = $object['status'] = 'succeeded';

        if ($success && $type === 'charge.succeeded') {
            $operator->accept();
            Event::fire('viamage.stripesubscriptions.charge.succeeded', $operator->getModel());
        }

        if ($success && $type === 'charge.failed') {
            $operator->cancel();
            Event::fire('viamage.stripesubscriptions.charge.failed', $operator->getModel());
        }

        if ($success && $type === 'charge.refunded') {
            $operator->refund();
            Event::fire('viamage.stripesubscriptions.charge.refunded', $operator->getModel());
        }
        if ($success && $type === 'charge.dispute.created') {
            // TODO support
            \Log::error('Unsupported incoming webhook of type '.$type.' '.print_r($object, true));
        }
        if ($success && $type === 'charge.dispute.closed') {
            // TODO support
            \Log::error('Unsupported incoming webhook of type '.$type.' '.print_r($object, true));
        }
        if ($success && $type === 'charge.updated') {
            // TODO support
            \Log::error('Unsupported incoming webhook of type '.$type.' '.print_r($object, true));
        }
        if ($success && $type === 'charge.pending') {
            // TODO support
            \Log::error('Unsupported incoming webhook of type '.$type.' '.print_r($object, true));
        }

        $this->setStripeKey();
        $payment = $operator->getModel();
        $uuid = $payment->uuid;
        $invoiceId = $object['invoice'];

        $invoice = Invoice::retrieve($invoiceId);
        \Log::info('Charge hook: PAYMENT '.$uuid);
        // TODO put some safety
        $subscriptionId = $invoice['subscription'];
        $subscription = Subscription::where('subscription_id', $subscriptionId)->first();
        \DB::table('viamage_stripesubscriptions_pay_pivot')->insert(
            ['subscription_id' => $subscription->id, 'payment_id' => $payment->id]
        );

        \Event::fire('viamage.stripesubscriptions.payment', [$subscription, $payment]);
    }

    private function processSubscriptionHook($object): Subscription
    {
        //    $this->validateWebhook();
        $subscriptionId = $object['id'];
        $subscription = Subscription::where('subscription_id', $subscriptionId)->first();
        $this->updateSubscriptionOnHook($subscription, $object);

        return $subscription;
    }

    private function createSubscriptionModel(\Stripe\Subscription $stripeObject, $user): Subscription
    {
        $isActive = $stripeObject->status === 'active';
        $hasTrial = (bool)$stripeObject->trial_end;
        $cancelAt = null;
        $canceledAt = null;
        if ($stripeObject->cancel_at_period_end) {
            $cancelAt = Carbon::createFromTimestamp($stripeObject->current_period_end);
        }
        if ($stripeObject->canceled_at) {
            Carbon::createFromTimestamp($stripeObject->canceled_at);
        }
        $subscription = new Subscription();
        $subscription->subscription_id = $stripeObject->id;
        $subscription->user_id = $user->id;
        $subscription->is_active = $isActive;
        $subscription->current_period_start = Carbon::createFromTimestamp($stripeObject->current_period_start);
        $subscription->current_period_end = Carbon::createFromTimestamp($stripeObject->current_period_end);
        $subscription->billing = $stripeObject->billing;
        $subscription->metadata = json_encode($stripeObject->metadata);
        $subscription->has_trial = $hasTrial;
        $subscription->trial_start = $stripeObject->trial_start;
        $subscription->trial_end = $stripeObject->trial_end;
        $subscription->cancel_at = $cancelAt;
        $subscription->canceled_at = $canceledAt;
        $subscription->save();
        \Event::fire('viamage.stripesubscriptions.change', [$subscription]);

        return $subscription;
    }

    private function generateChargeDescription($object)
    {
        return $object['statement_descriptor'].' '.Carbon::now()->toDateString();
    }

    private function calculateValue(\Stripe\Subscription $stripeObject)
    {
        $items = $stripeObject->items->all();
        $values = [];
        $defaultCurrency = $this->currency;
        /** @var Money $result */
        $result = Money::$defaultCurrency(0);
        /** @var \Stripe\SubscriptionItem $item */
        foreach ($items as $item) {
            $currency = strtoupper($item->plan->currency);
            if ($currency !== $defaultCurrency) {
                // TODO calculate
            }
            $values[] = Money::$currency($item->plan->amount);
        }

        foreach ($values as $currency => $value) {
            $money = Money::$currency($value);
            $result = $result->add($money);
        }

        return $result;
    }

    private function updateSubscriptionOnHook(Subscription $subscription = null, array $object)
    {
        if (!$subscription) {
            \Log::error('Subscription not found!');

            return null;
        }
        $active = $object['status'] === 'active';
        $hasTrial = false;
        $trialStart = null;
        $trialEnd = null;
        $cancelAt = null;
        $canceledAt = null;
        if ($object['cancel_at_period_end']) {
            $cancelAt = Carbon::createFromTimestamp($object['current_period_end']);
        }
        if ($object['canceled_at']) {
            $canceledAt = Carbon::createFromTimestamp($object['canceled_at']);
        }
        if ($object['trial_start']) {
            $hasTrial = true;
            $trialStart = Carbon::createFromTimestamp($object['trial_start']);
            $trialEnd = Carbon::createFromTimestamp($object['trial_end']);
        }
        $subscription->is_active = $active;
        $subscription->current_period_start = Carbon::createFromTimestamp($object['current_period_start']);
        $subscription->current_period_end = Carbon::createFromTimestamp($object['current_period_end']);
        $subscription->billing = $object['billing'];
        $subscription->metadata = json_encode($object['metadata']);
        $subscription->has_trial = $hasTrial;
        $subscription->trial_start = $trialStart;
        $subscription->trial_end = $trialEnd;
        $subscription->cancel_at = $cancelAt;
        $subscription->canceled_at = $canceledAt;
        $subscription->save();
        EventEmitter::emit(
            'viamage.stripesubscriptions::subscription.saved',
            [
                'topic'   => 'subs_'.$subscription->user->persist_code,
                'user_id' => $subscription->user->id,
                'command' => 'refreshCallbacksTable',
            ],
            true
        );
    }

    private function validateWebhook()
    {
        $this->setStripeKey();
        $endpoint_secret = $this->webhookSecret;

        $payload = @file_get_contents('php://input');
        $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
        $event = null;

        try {
            \Stripe\Webhook::constructEvent(
                $payload,
                $sig_header,
                $endpoint_secret
            );
        } catch (\UnexpectedValueException $e) {
            // Invalid payload

            throw $e; //todo
        } catch (\Stripe\Error\SignatureVerification $e) {
            // Invalid signature
            throw $e; //todo;
        }
    }

    private function setStripeKey()
    {
        if ($this->testMode) {
            \Stripe\Stripe::setApiKey($this->stestApiKey);
        } else {
            \Stripe\Stripe::setApiKey($this->sapiKey);
        }

    }

}