<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 5/2/18
 * Time: 1:04 PM
 */

namespace Viamage\StripeSubscriptions\Classes;

use Keios\ProUser\Models\User;
use Viamage\RealTime\Classes\Pusher;

/**
 * Class EventEmitter
 * @package Viamage\StripeSubscriptions\Classes
 */
class EventEmitter
{
    /**
     * @var string
     */
    public $channel;
    /**
     * @var User
     */
    public $user;

    public static function emit($name, $payload, $push = false)
    {
        if ($push) {
            Pusher::push(
                $payload
            );
        }
        \Event::fire($name, $payload);
    }

}