<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/29/18
 * Time: 6:17 PM
 */

namespace Viamage\StripeSubscriptions\Classes;

use Keios\PaymentGateway\ValueObjects\AdjustableItem;

class SubscriptionItem extends AdjustableItem
{
    const TYPE = 'SubscriptionItem';
}