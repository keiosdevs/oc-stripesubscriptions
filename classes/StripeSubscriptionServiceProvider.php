<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/29/18
 * Time: 6:18 PM
 */

namespace Viamage\StripeSubscriptions\Classes;

use Illuminate\Support\ServiceProvider;

class StripeSubscriptionServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $app = $this->app;

        $this->app->singleton('stripesubscription.cartmanager', function () use ($app) {
            $sessionCartStore = new SessionCartStore($app->make('session'));

            return new CartManager($sessionCartStore);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'rechargeshop.cartmanager'
        ];
    }
}