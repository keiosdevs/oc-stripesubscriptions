<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 4/30/18
 * Time: 11:25 AM
 */

namespace Viamage\StripeSubscriptions\Events;
use Event;
use Illuminate\Broadcasting\Channel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
class SubscriptionCanceled extends Event implements ShouldBroadcast
{

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|Channel[]
     */
    public function broadcastOn()
    {

    }
}