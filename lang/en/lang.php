<?php

return [
    'subscription'  => [
        'new'           => 'New Subscription',
        'label'         => 'Subscription',
        'create_title'  => 'Create Subscription',
        'update_title'  => 'Edit Subscription',
        'preview_title' => 'Preview Subscription',
        'list_title'    => 'Manage Subscriptions',
    ],
    'subscriptions' => [
        'delete_selected_confirm' => 'Delete the selected Subscriptions?',
        'menu_label'              => 'Subscriptions',
        'return_to_list'          => 'Return to Subscriptions',
        'delete_confirm'          => 'Do you really want to delete this Subscription?',
        'delete_selected_success' => 'Successfully deleted the selected Subscriptions.',
        'delete_selected_empty'   => 'There are no selected Subscriptions to delete.',
    ],
    'messages'      => [
        'successfully_subscribed' => 'Subscription is active!',
    ],
    'strings'       => [
        'payments' => 'Payments',
    ],
];